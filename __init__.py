# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool

from . import configuration
from . import employee_team
from . import team_timesheet
from . import timesheet


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationSequence,
        team_timesheet.EditWorkTimesheetStart,
        team_timesheet.EditEmployeeTimesheetsStart,
        employee_team.Employee,
        team_timesheet.TeamTimesheet,
        team_timesheet.TeamTimesheetEmployee,
        team_timesheet.TeamTimesheetWork,
        timesheet.WorkTeam,
        timesheet.Work,
        employee_team.Team,
        timesheet.Timesheet,
        team_timesheet.TimesheetTeamWorkCompanyEmployee,
        module='team_timesheet', type_='model')
    Pool.register(
        team_timesheet.EditWorkTimesheets,
        team_timesheet.EditEmployeeTimesheets,
        module='team_timesheet', type_='wizard')
