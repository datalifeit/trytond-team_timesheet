# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView, ModelSQL, ModelSingleton
from trytond.pool import Pool
from trytond.pyson import Eval, Id
from trytond.tools.multivalue import migrate_property
from trytond.modules.company.model import (
    CompanyValueMixin, CompanyMultiValueMixin)
from trytond import backend

__all__ = ['Configuration', 'ConfigurationSequence']


class Configuration(ModelSingleton, ModelSQL, ModelView,
            CompanyMultiValueMixin):
    """Timesheet Configuration"""
    __name__ = 'timesheet.configuration'

    team_timesheet_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Team timesheet Sequence',
            domain=[
                ('sequence_type', '=', Id('team_timesheet',
                    'sequence_type_team_timesheet')),
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None])
            ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'team_timesheet_sequence':
            return pool.get('timesheet.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_team_timesheet_sequence(cls, **pattern):
        return cls.multivalue_model(
            'team_timesheet_sequence').default_team_timesheet_sequence()


class ConfigurationSequence(ModelSQL, CompanyValueMixin):
    "Configuration Sequence"
    __name__ = 'timesheet.configuration.sequence'

    team_timesheet_sequence = fields.Many2One('ir.sequence',
        'Team timesheet Sequence',
        domain=[
            ('sequence_type', '=', Id('team_timesheet',
               'sequence_type_team_timesheet')),
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None])
        ])

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)

        super(ConfigurationSequence, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('team_timesheet_sequence')
        value_names.append('team_timesheet_sequence')
        fields.append('company')
        migrate_property(
            'timesheet.configuration', field_names, cls, value_names,
            fields=fields)

    @classmethod
    def default_team_timesheet_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('team_timesheet',
                'sequence_team_timesheet')
        except KeyError:
            return None
