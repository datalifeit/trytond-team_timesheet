=======================
Team Timesheet Scenario
=======================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from trytond.modules.company_employee_team.tests.tools import \
    ...     create_team, get_team
    >>> from proteus import Model, Wizard
    >>> from datetime import timedelta
    >>> from dateutil.relativedelta import relativedelta


Install team_timesheet Module::

    >>> config = activate_modules('team_timesheet')


Get today date::

    >>> import datetime
    >>> today = datetime.date.today()


Get Models::

    >>> Employee = Model.get('company.employee')
    >>> Group = Model.get('res.group')
    >>> Party = Model.get('party.party')
    >>> TeamTimesheet = Model.get('timesheet.team.timesheet')
    >>> Timesheet = Model.get('timesheet.team.work-company.employee')
    >>> TimesheetLine = Model.get('timesheet.line')
    >>> User = Model.get('res.user')
    >>> Work = Model.get('timesheet.work')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Reload the context::

    >>> config._context = User.get_preferences(True, config.context)


Create team::

    >>> team = create_team()
    >>> employee, = team.employees


Create works::

    >>> work1 = Work(name='Work1')
    >>> work1.save()


Create a team timesheet::

    >>> team_timesheet = TeamTimesheet(date=today)
    >>> team_timesheet.num_workers = 5
    >>> team_timesheet.team = team
    >>> team_timesheet.num_workers == None
    True
    >>> team_timesheet.date = today
    >>> team_timesheet.save()
    >>> team_timesheet.code == '1'
    True
    >>> team_timesheet.employees[0].employee.party.name == 'Pepito Perez'
    True
    >>> team_timesheet.state == 'draft'
    True


Add a work to a team timesheet::

    >>> tts_work = team_timesheet.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.duration = timedelta(hours=7)
    >>> team_timesheet.save()
    >>> tts_work.planned_duration.total_seconds() / 3600
    7.0

    >>> tts_work, = [w for w in team_timesheet.works]
    >>> tts_employee, = [w for w in team_timesheet.employees]


Check edit work timesheets all_employees::

    >>> edit_work_timesheets = Wizard('timesheet.team.timesheet-timesheet.work.edit_timesheets', [tts_work])
    >>> edit_work_timesheets.form.work == work1
    True
    >>> edit_work_timesheets.form.duration == timedelta(hours=7)
    True
    >>> edit_work_timesheets.form.total_duration == timedelta(hours=7)
    True
    >>> edit_work_timesheets.form.time_type
    'employee'
    >>> edit_work_timesheets.form.allowed_employees == [employee]
    True


Check timesheets::

    >>> timesheet, = edit_work_timesheets.form.timesheets
    >>> timesheet.team_timesheet == team_timesheet
    True
    >>> timesheet.currency == company.currency
    True
    >>> timesheet.tts_date == team_timesheet.date
    True
    >>> timesheet.tts_work == tts_work
    True
    >>> timesheet.tts_employee == tts_employee
    True
    >>> timesheet.work == work1
    True
    >>> timesheet.employee == employee
    True
    >>> timesheet.duration == timedelta(hours=7)
    True
    >>> timesheet.total_hours
    7.0
    >>> bool(timesheet.timesheet_line)
    False


Update duration in edit work timesheets::

    >>> edit_work_timesheets.form.duration = timedelta(hours=8)
    >>> edit_work_timesheets.form.total_duration == timedelta(hours=8)
    True

    >>> timesheet.duration == timedelta(hours=8)
    True
    >>> timesheet.total_hours
    8.0

    >>> edit_work_timesheets.execute('edit_timesheets')


Check team timesheet::

    >>> team_timesheet = TeamTimesheet(team_timesheet.id)

    >>> tts_work, = team_timesheet.works
    >>> tts_work.duration == timedelta(hours=8)
    True
    >>> tts_work.total_duration == timedelta(hours=8)
    True
    >>> tts_work.total_hours
    8.0

    >>> tts_employee, = team_timesheet.employees
    >>> tts_employee.duration == timedelta(hours=8)
    True
    >>> tts_employee.day_duration == timedelta(hours=8)
    True


Modify one employee timesheet::

    >>> edit_employee_timesheets = Wizard('timesheet.team.timesheet-company.employee.edit_timesheets', [tts_employee])
    >>> edit_employee_timesheets.form.employee == employee
    True
    >>> edit_employee_timesheets.form.day_duration == timedelta(hours=8)
    True

    >>> timesheet, = edit_employee_timesheets.form.timesheets
    >>> timesheet.duration == timedelta(hours=8)
    True
    >>> timesheet.duration = timedelta(hours=4)
    >>> edit_employee_timesheets.execute('edit_timesheets')

    >>> team_timesheet = TeamTimesheet(team_timesheet.id)
    >>> tts_work, = team_timesheet.works
    >>> tts_employee, = team_timesheet.employees
    >>> tts_work.total_duration.total_seconds() / 3600
    4.0
    >>> tts_work.total_hours
    4.0
    >>> tts_work.duration.total_seconds() / 3600
    8.0
    >>> tts_employee.duration.total_seconds() / 3600
    4.0


Check searcher in timesheet::

    >>> len(Timesheet.find([
    ...  ('team_timesheet', '=', team_timesheet)]))
    1
    >>> len(Timesheet.find([
    ...  ('team_timesheet', '=', None)]))
    0
    >>> len(Timesheet.find([
    ...  ('employee', '=', tts_employee.employee)]))
    1
    >>> len(Timesheet.find([
    ...  ('employee', '=', None)]))
    0
    >>> len(Timesheet.find([
    ...  ('tts_date', '=', team_timesheet.date)]))
    1
    >>> len(Timesheet.find([
    ...  ('tts_date', '=', None)]))
    0
    >>> len(Timesheet.find([
    ...  ('tts_date', '=', today + relativedelta(days=1))]))
    0


Check when employee is deleted so too do its timesheets::

    >>> timesheet, = Timesheet.find([
    ...     ('team_timesheet', '=', team_timesheet)])
    >>> timesheet.tts_employee == team_timesheet.employees[0]
    True
    >>> team_timesheet.employees[0].delete()
    >>> team_timesheet.save()

    >>> timesheet = Timesheet.find([
    ...     ('team_timesheet', '=', team_timesheet)])
    >>> bool(timesheet)
    False


Workflow progress::

    >>> team_timesheet2 = TeamTimesheet(date=today, team=team)
    >>> tts_work = team_timesheet2.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'team'
    >>> tts_work.duration = timedelta(hours=8)
    >>> team_timesheet2.click('wait')
    >>> team_timesheet2.click('confirm')
    >>> team_timesheet2.click('do')
    >>> team_timesheet2.reload()
    >>> team_timesheet2.works[0].total_duration.total_seconds() / 3600
    8.0


Check line when state is done and timesheet::

    >>> tts_work ,= team_timesheet2.works

    >>> team_timesheet2.state
    'done'
    >>> timesheet_line, = team_timesheet2.timesheets
    >>> timesheet_line.duration == timedelta(hours=8)
    True
    >>> timesheet_line.work == work1
    True
    >>> timesheet_line.tts_work == tts_work
    True
    >>> timesheet_line.employee == employee
    True

    >>> timesheet, = Timesheet.find([
    ...     ('team_timesheet', '=', team_timesheet2)])
    >>> timesheet.timesheet_line == timesheet_line
    True


Team timesheet delete in state done::

    >>> team_timesheet2.delete() #doctest: +ELLIPSIS
    Traceback (most recent call last):
      ...
    trytond.exceptions.UserError: Team timesheet "..." must be cancelled before deletion. - 


Return to draft team timesheet::

    >>> team_timesheet2.click('draft')
    >>> len(team_timesheet2.timesheets)
    0

    >>> timesheet, = Timesheet.find([
    ...     ('team_timesheet', '=', team_timesheet2)])
    >>> bool(timesheet.timesheet_line)
    False


Create new employee to change the timesheet::

    >>> new_party = Party(name='New Pepito')
    >>> new_party.save()

    >>> new_employee = Employee(company=company, party=new_party)
    >>> new_employee.save()


Check change employee in team timesheet::

    >>> team_timesheet2.click('wait')
    >>> team_timesheet2.employees[0].employee = new_employee
    >>> team_timesheet2.save()

    >>> timesheet, = Timesheet.find([
    ...     ('team_timesheet', '=', team_timesheet2)])
    >>> timesheet.employee == new_employee
    True


Create work2::

    >>> work2 = Work(name='Work 2')
    >>> work2.save()


Check change work in team timesheet::

    >>> team_timesheet2.click('wait')
    >>> team_timesheet2.works[0].work = work2
    >>> team_timesheet2.save()

    >>> timesheet, = Timesheet.find([
    ...     ('team_timesheet', '=', team_timesheet2)])
    >>> timesheet.work == work2
    True

    >>> team_timesheet2.click('confirm')
    >>> team_timesheet2.click('do')
    >>> team_timesheet2.done_date == today
    True


Check duplicated timesheets::

    >>> duplicated, = team_timesheet2.duplicate()
    >>> bool(duplicated.done_date)
    False
    >>> duplicated.click('wait')
    >>> duplicated.click('confirm')
    >>> duplicated.click('do') #doctest: +ELLIPSIS
    Traceback (most recent call last):
    	...
    trytond.exceptions.UserWarning: Team "A Team" has already a team timesheet on "...". - 

Check team work domain on Timesheet work::

    >>> team.works.append(work2)
    >>> team.save()
    >>> team = get_team()

    >>> team_timesheet3 = TeamTimesheet(date=today, team=team)
    >>> tts_work = team_timesheet3.works.new()
    >>> tts_work.work == work2
    True
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'team'
    >>> tts_work.duration = timedelta(hours=8)
    >>> team_timesheet3.save()
    Traceback (most recent call last):
    	...
    trytond.model.modelstorage.DomainValidationError: The value for field "Work" in "TeamTimesheet - Work" is not valid according to its domain. - 
    >>> _ = team.works.pop()
    >>> team.save()
    >>> team = get_team()
    >>> team_timesheet3.save()


Check duplicate works::

    >>> team_timesheet4 = TeamTimesheet(date=today, team=team)
    >>> tts_work = team_timesheet4.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.duration = timedelta(hours=8)

    >>> tts_work = team_timesheet4.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.duration = timedelta(hours=5)
    >>> team_timesheet4.save() #doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: "Work1" work of teamtimesheet "..." must be unique. - 
    >>> tts_work.work = work2
    >>> team_timesheet4.save()

Do team timesheet::

    >>> team_timesheet4.click('wait')
    >>> team_timesheet4.click('confirm')
    >>> team_timesheet4.click('do') #doctest: +ELLIPSIS
    Traceback (most recent call last):
      ...
    trytond.exceptions.UserWarning: Team "A Team" has already a team timesheet on "...". - 
    >>> warning_name = 'duplicated_timesheet_%s_%s' % (team, today.strftime('%m/%d/%Y'))
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name=warning_name, always=True).save()
    >>> team_timesheet4.click('do')


Check timesheets in team timesheet::

    >>> len(team_timesheet4.timesheets)
    2
    >>> _timesheet_work1, = [timesheet for timesheet in team_timesheet4.timesheets
    ...     if timesheet.work == work1]
    >>> _timesheet_work2, = [timesheet for timesheet in team_timesheet4.timesheets
    ...     if timesheet.work == work2]

    >>> timesheets = Timesheet.find([
    ...     ('team_timesheet', '=', team_timesheet4)])
    >>> len(timesheets)
    2

    >>> timesheet_work1, = [timesheet for timesheet in timesheets
    ...     if timesheet.work == work1]
    >>> timesheet_work1.duration.total_seconds() / 3600
    8.0
    >>> timesheet_work1.total_hours
    8.0
    >>> timesheet_work1.timesheet_line == _timesheet_work1
    True
    >>> _timesheet_work1.total_hours
    8.0
    >>> timesheet_work1.tts_work == _timesheet_work1.tts_work
    True

    >>> timesheet_work2, = [timesheet for timesheet in timesheets
    ...     if timesheet.work == work2]
    >>> timesheet_work2.duration.total_seconds() / 3600
    5.0
    >>> timesheet_work2.total_hours
    5.0
    >>> timesheet_work2.timesheet_line == _timesheet_work2
    True
    >>> _timesheet_work2.total_hours
    5.0
    >>> timesheet_work2.tts_work == _timesheet_work2.tts_work
    True


Create team timesheet without team::

    >>> team_timesheet5 = TeamTimesheet(date=today, num_workers=7)
    >>> tts_work = team_timesheet5.works.new()
    >>> tts_work.work = work1
    >>> tts_work.duration = timedelta(hours=3)
    >>> tts_work.planned_duration.total_seconds() / 3600
    21.0
    >>> tts_work = team_timesheet5.works.new()
    >>> tts_work.work = work2
    >>> tts_work.duration = timedelta(hours=7)
    >>> tts_work.planned_duration.total_seconds() / 3600
    49.0
    >>> team_timesheet5.save()
    >>> team_timesheet5.click('wait')
    >>> team_timesheet5.click('confirm')
    >>> team_timesheet5.click('do')


Workflow progress with time = 0h::

    >>> team_timesheet6 = TeamTimesheet(date=today, team=team)
    >>> tts_work2 = team_timesheet6.works.new()
    >>> tts_work2.work = work1
    >>> tts_work2.time_type = 'team'
    >>> tts_work2.duration = timedelta(hours=0)
    >>> team_timesheet6.click('wait')
    >>> team_timesheet6.click('confirm')
    >>> team_timesheet6.click('do')
    >>> len(team_timesheet6.timesheets)
    0


Check Delete timesheet lines with 0h at confirmation::

    >>> team_timesheet7 = TeamTimesheet(date=today, team=team)
    >>> team_timesheet7.date = today + relativedelta(days=1)
    >>> tts_work = team_timesheet7.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'team'
    >>> tts_work.duration = timedelta(hours=8)
    >>> team_timesheet7.click('wait')

    >>> tts_work = team_timesheet7.works[0]
    >>> edit_work_timesheets = Wizard('timesheet.team.timesheet-timesheet.work.edit_timesheets', [tts_work])
    >>> edit_work_timesheets.form.total_duration == timedelta(hours=8)
    True
    >>> edit_work_timesheets.form.duration == timedelta(hours=8)
    True
    >>> edit_work_timesheets.form.planned_duration == timedelta(hours=8)
    True
    >>> timesheet, = edit_work_timesheets.form.timesheets
    >>> timesheet.duration = timedelta(0)
    >>> edit_work_timesheets.form.total_duration == timedelta(0)
    True
    >>> edit_work_timesheets.form.duration == timedelta(hours=8)
    True
    >>> edit_work_timesheets.form.planned_duration == timedelta(hours=8)
    True
    >>> edit_work_timesheets.execute('edit_timesheets')

    >>> team_timesheet7.click('confirm')
    >>> team_timesheet7.click('do')
    >>> len(team_timesheet7.timesheets)
    0


Add employee in team::

    >>> add = team.employee_allocations.new()
    >>> add.employee = new_employee
    >>> team.save()
    >>> len(team.employees)
    2


Create team timesheet and check changes un time_type::

    >>> team_timesheet8 = TeamTimesheet(date=today, team=team)
    >>> tts_work = team_timesheet8.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'team'
    >>> tts_work.duration = timedelta(hours=8)
    >>> team_timesheet8.save()
    >>> tts_work, = team_timesheet8.works
    >>> tts_work.total_duration == timedelta(hours=8)
    True
    >>> tts_work.planned_duration == timedelta(hours=8)
    True
    >>> tts_work.time_type = 'employee'
    >>> team_timesheet8.save()
    >>> tts_work, = team_timesheet8.works
    >>> tts_work.total_duration == timedelta(hours=16)
    True
    >>> tts_work.planned_duration == timedelta(hours=16)
    True


Change duration in edit work timesheets::

    >>> edit_work_timesheets = Wizard('timesheet.team.timesheet-timesheet.work.edit_timesheets', [tts_work])
    >>> edit_work_timesheets.form.duration = timedelta(hours=4)
    >>> edit_work_timesheets.execute('edit_timesheets')

    >>> team_timesheet8.reload()
    >>> tts_work, = team_timesheet8.works
    >>> tts_work.duration == timedelta(hours=4)
    True
    >>> tts_work.total_duration == timedelta(hours=8)
    True
    >>> tts_work.planned_duration == timedelta(hours=8)
    True


    >>> edit_work_timesheets = Wizard('timesheet.team.timesheet-timesheet.work.edit_timesheets', [tts_work])
    >>> timesheet, = [t for t in  edit_work_timesheets.form.timesheets
    ...     if t.employee == new_employee]
    >>> timesheet.duration = timedelta(hours=8)
    >>> edit_work_timesheets.execute('edit_timesheets')

    >>> team_timesheet8.reload()
    >>> tts_work, = team_timesheet8.works
    >>> tts_work.duration == timedelta(hours=4)
    True
    >>> tts_work.total_duration == timedelta(hours=12)
    True
    >>> tts_work.planned_duration == timedelta(hours=8)
    True


Check timesheet lines::

    >>> team_timesheet8.click('wait')
    >>> team_timesheet8.click('confirm')
    >>> team_timesheet8.click('do')
    >>> len(team_timesheet8.timesheets)
    2

    >>> timesheet_employee, = [timesheet for timesheet in team_timesheet8.timesheets
    ...     if timesheet.employee == employee]
    >>> timesheet_employee.duration == timedelta(hours=4)
    True

    >>> timesheet_new_employee, = [timesheet for timesheet in team_timesheet8.timesheets
    ...     if timesheet.employee == new_employee]
    >>> timesheet_new_employee.duration == timedelta(hours=8)
    True
