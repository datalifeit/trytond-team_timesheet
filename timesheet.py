# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.modules.company_employee_team.employee_team import \
    EmployeeOrderMixin
from trytond.pyson import Eval, Equal
from trytond.exceptions import UserError
from trytond.i18n import gettext


class WorkTeam(ModelSQL, ModelView):
    """Work Team"""
    __name__ = 'timesheet.work-company.employee.team'
    _table = 'timesheet_work_team_rel'

    work = fields.Many2One('timesheet.work', 'Work', required=True,
        select=True)
    team = fields.Many2One('company.employee.team', 'Team',
        required=True, select=True)


class Work(metaclass=PoolMeta):
    __name__ = 'timesheet.work'

    teams = fields.Many2Many('timesheet.work-company.employee.team',
        'work', 'team', 'Teams')


# TODO: Set readonly lines with team_timesheet in timesheet.line
# from timesheet module
class Timesheet(EmployeeOrderMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    team_timesheet = fields.Function(
        fields.Many2One('timesheet.team.timesheet', 'Team timesheet'),
        'get_team_timesheet', searcher='search_team_timesheet')
    tts_work = fields.Many2One('timesheet.team.timesheet-timesheet.work',
        'Team Timesheet Work', readonly=True, select=True, ondelete='CASCADE')
    tts_work_summary = fields.Function(fields.Char('Team Timesheet Work'),
        'get_tts_work_summary')
    tts_state = fields.Function(
        fields.Selection('get_tts_states', 'Team timesheet state'),
        'get_tts_state')

    @classmethod
    def __setup__(cls):
        super(Timesheet, cls).__setup__()
        readonly = Equal(Eval('tts_state', ''), 'done')
        for _field in cls.get_readonly_fields():
            field = getattr(cls, _field, None)
            if field:
                if 'readonly' in field.states:
                    field.states['readonly'] |= readonly
                else:
                    field.states['readonly'] = readonly

                field.depends.append('tts_state')

        cls._deny_modify_fields = set(['company',
            'employee', 'date', 'work', 'duration'])

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()
        tl = cls.__table__()
        pool = Pool()
        TTSW = pool.get('timesheet.team.timesheet-timesheet.work')
        ttsw = TTSW.__table__()
        super(Timesheet, cls).__register__(module_name)

        if table.column_exist('team_timesheet'):
            values = tl.join(ttsw,
                condition=(ttsw.team_timesheet == tl.team_timesheet)
                    & (ttsw.work == tl.work)
            ).select(
                ttsw.id,
                where=(sql_table.id == tl.id))
            cursor.execute(*sql_table.update(
                [sql_table.tts_work],
                [values]))
            table.drop_column('team_timesheet')

    def get_tts_work_summary(self, name):
        if self.tts_work:
            return self.tts_work.rec_name

    @classmethod
    def search_team_timesheet(cls, name, clause):
        if clause[2] is None:
            return [('tts_work',) + tuple(clause[1:])]
        return [('tts_work.%s' % clause[0],) + tuple(clause[1:])]

    @classmethod
    def get_team_timesheet(cls, records, name):
        return {r.id: r.tts_work.team_timesheet.id if
            r.tts_work else None for r in records}

    def get_tts_state(self, name=None):
        if self.team_timesheet:
            return self.team_timesheet.state

        return None

    @classmethod
    def get_tts_states(cls):
        pool = Pool()
        TeamTimesheet = pool.get('timesheet.team.timesheet')

        states = TeamTimesheet.fields_get(['state'])['state']['selection']
        return states + [(None, '')]

    @classmethod
    def get_readonly_fields(cls):
        return ['employee', 'company', 'date', 'duration', 'work']

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        new_args = []
        for lines, values in zip(actions, actions):
            vals_set = set(values)
            if vals_set & cls._deny_modify_fields:
                new_lines = []
                for line in lines:
                    if not line.team_timesheet:
                        continue
                    if line.team_timesheet.check_timesheet_changes(line):
                        new_lines.append(line)
                new_args.extend([new_lines, values])
            else:
                new_args.extend([lines, values])
        super(Timesheet, cls).write(*new_args)

    @classmethod
    def delete(cls, records):
        context = Transaction().context
        for record in records:
            if (record.team_timesheet
                    and not context.get('allow_delete_line', False)):
                raise UserError(gettext(
                    'team_timesheet.msg_timesheet_line_delete',
                    line=record.rec_name,
                    team_timesheet=record.team_timesheet.rec_name))
        super().delete(records)
