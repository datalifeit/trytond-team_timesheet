# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import timedelta
from itertools import groupby

from sql import Literal, Null, With
from sql.aggregate import Count, Sum
from sql.conditionals import Case
from sql.functions import CharLength
from trytond import backend
from trytond.exceptions import UserError, UserWarning
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Unique, Workflow, fields
from trytond.modules.company_employee_team.employee_team import \
    EmployeeOrderMixin
from trytond.modules.timesheet_absolute_hours import get_absolute_hours_time
from trytond.pool import Pool
from trytond.pyson import Bool, Equal, Eval, If, Not, In
from trytond.transaction import Transaction
from trytond.wizard import (Button, StateTransition, StateView,
    Wizard, StateAction)
from trytond.tools import reduce_ids


class TeamTimesheet(Workflow, ModelView, ModelSQL):
    """Team Timesheet"""
    __name__ = 'timesheet.team.timesheet'
    _rec_name = 'code'

    code = fields.Char('Code', required=True, select=True,
        states={'readonly': Eval('code_readonly', True)},
        depends=['code_readonly'])
    code_readonly = fields.Function(
        fields.Boolean('Code Readonly'),
        'get_code_readonly')
    date = fields.Date('Date', required=True, select=True,
        states={
            'readonly': ~Eval('state').in_(['draft', 'waiting'])},
        depends=['state'])
    team = fields.Many2One('company.employee.team', 'Team', select=True,
        domain=[('company', '=', Eval('company'))],
        states={
            'readonly':
                Not(Equal(Eval('state'), 'draft'))
                | Bool(Eval('works'))
                | Bool(Eval('employees')),
            'required': Bool(~Eval('num_workers'))},
        context={'date': Eval('date')},
        depends=['company', 'works', 'state', 'employees', 'date',
            'num_workers'])
    description = fields.Text('Description', depends=['date'])
    timesheets = fields.Function(
        fields.Many2Many('timesheet.line', None, None, 'Timesheets'),
        'get_timesheets', searcher='search_timesheets')
    works = fields.One2Many(
        'timesheet.team.timesheet-timesheet.work', 'team_timesheet', 'Works',
        depends=['allowed_employees', 'state'],
        context={
            'allowed_employees': Eval('allowed_employees'),
            'absolute_hours_time': get_absolute_hours_time()},
        states={
            'readonly': ~Eval('state').in_(['draft', 'waiting']),
            'required': Eval('state').in_(['confirmed', 'done'])})
    allowed_works = fields.Function(
        fields.Many2Many(
            'timesheet.work', None, None, 'Allowed works',
            readonly=True, required=False),
        'on_change_with_allowed_works')
    employees = fields.One2Many(
        'timesheet.team.timesheet-company.employee', 'team_timesheet',
        'Employees',
        states={
            'readonly': ~Eval('state').in_(['draft', 'waiting']),
            'required': (Eval('state').in_(['confirmed', 'done']) &
                Bool(~Eval('num_workers'))),
            'invisible': Bool(Eval('num_workers'))},
        context={
            'absolute_hours_time': get_absolute_hours_time()},
        order=[('employee', 'ASC')],
        depends=['state', 'num_workers'])
    num_workers = fields.Integer("Number of workers",
        domain=['OR',
            ('num_workers', '>', 0),
            ('num_workers', '=', None)],
        states={
            'readonly': Eval('state') != 'draft',
            'required': Bool(~Eval('employees')),
            'invisible': Bool(Eval('employees'))
        },
        depends=['team', 'employees', 'state', 'works'])
    allowed_employees = fields.Function(
        fields.Many2Many('company.employee', None, None, 'Allowed employees',
            readonly=True, required=False),
        'on_change_with_allowed_employees')
    company = fields.Many2One(
        'company.company', 'Company',
        select=True, required=True,
        states={'readonly':
            Not(Equal(Eval('state'), 'draft')) | Bool(Eval('team')) |
            Bool(Eval('works')) | Bool(Eval('have_employees'))},
        depends=['state', 'works', 'have_employees', 'team'])
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting', 'Waiting'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled')], 'State',
        readonly=True, required=True)
    have_employees = fields.Function(
        fields.Boolean('Have employees'),
        'get_have_employees')
    done_date = fields.Date('Done date', readonly=True)

    @classmethod
    def __setup__(cls):
        super(TeamTimesheet, cls).__setup__()
        cls._order = [
            ('date', 'DESC'),
            ('id', 'DESC'),
            ]
        cls._transitions |= set((
                ('draft', 'waiting'),
                ('waiting', 'confirmed'),
                ('confirmed', 'waiting'),
                ('confirmed', 'done'),
                ('waiting', 'draft'),
                ('draft', 'cancelled'),
                ('cancelled', 'draft'),
                ('done', 'draft')
                ))
        cls._buttons.update({
            'cancel': {
                'invisible': Eval('state') != 'draft',
                'depends': ['state']
            },
            'draft': {
                'invisible': ~Eval('state').in_(['cancelled', 'waiting', 'done']),
                'icon': If(Eval('state') == 'cancelled', 'tryton-undo',
                    'tryton-back'),
                'depends': ['state']
            },
            'wait': {
                'invisible': Not(In(Eval('state'), ['draft', 'confirmed'])),
                'icon': If(Eval('state') == 'draft', 'tryton-forward',
                    'tryton-back'),
                'depends': ['state']
            },
            'confirm': {
                'invisible': Eval('state') != 'waiting',
                'depends': ['state'],
            },
            'do': {
                'invisible': Eval('state') != 'confirmed',
                'depends': ['state']
            }
        })
        cls._order.insert(0, ('date', 'ASC'))
        cls._order.insert(1, ('team', 'ASC'))

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        sql_table = cls.__table__()
        cursor = Transaction().connection.cursor()

        super().__register__(module_name)

        table.not_null_action('team', action='remove')

        # Migration from 5.6: rename state cancel to cancelled
        cursor.execute(*sql_table.update(
                [sql_table.state], ['cancelled'],
                where=sql_table.state == 'cancel'))

        table.drop_constraint('code_uk1')

    @staticmethod
    def order_code(tables):
        table, _ = tables[None]
        return [CharLength(table.code), table.code]

    @staticmethod
    def default_code_readonly():
        model_config = Pool().get('timesheet.configuration')
        config = model_config(1)
        return bool(config.team_timesheet_sequence)

    @classmethod
    def get_timesheets(cls, records, name):
        return {r.id: [t.id for w in r.works for t in w.timesheets]
            for r in records}

    def get_code_readonly(self, name):
        return True

    @classmethod
    def create(cls, vlist):
        Config = Pool().get('timesheet.configuration')

        config = Config(1)
        default_company = cls.default_company()
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('code'):
                values['code'] = config.get_multivalue(
                    'team_timesheet_sequence',
                    company=values.get('company', default_company)).get()
        return super(TeamTimesheet, cls).create(vlist)

    @classmethod
    def copy(cls, team_timesheet, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['code'] = None
        default['timesheets'] = None
        default['done_date'] = None
        return super(TeamTimesheet, cls).copy(team_timesheet, default=default)

    @classmethod
    def delete(cls, timesheets):
        # Cancel before delete
        cls.cancel(timesheets)
        for sheet in timesheets:
            if sheet.state != 'cancelled':
                raise UserError(gettext(
                    'team_timesheet.msg_timesheet_team_timesheet_delete_cancel',
                    sheet=sheet.rec_name))
        super(TeamTimesheet, cls).delete(timesheets)

    @staticmethod
    def default_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @staticmethod
    def default_team():
        return Transaction().context.get('team', None)

    @classmethod
    def search_timesheets(cls, name, clause):
        text = 'works.{}'.format(name)
        return [(text,) + tuple(clause[1:])]

    @fields.depends('employees', 'team', 'date', 'state')
    def _extend_employees(self):
        TTsEmployee = Pool().get('timesheet.team.timesheet-company.employee')
        employees = {tse.employee.id: tse for tse in self.employees}
        new_employees = []
        if self.team:
            with Transaction().set_context(date=self.date):
                for employee_id in self.team.get_employees():
                    if employee_id in employees:
                        new_employees.append(employees[employee_id])
                    else:
                        new_employees.append(
                            TTsEmployee(
                                employee=employee_id,
                                tts_state=self.state))
        self.employees = new_employees

    @fields.depends(methods=['_extend_employees'])
    def on_change_team(self):
        self._extend_employees()

        self.num_workers = None

    @fields.depends('works', methods=['_extend_employees'])
    def on_change_date(self):
        self._extend_employees()
        for work in self.works:
            work._update_date(self.date)

    @fields.depends('works')
    def on_change_with_allowed_works(self, name='allowed_works'):
        if self.works:
            return list(set([w.work.id for w in self.works if w.work]))
        return []

    @fields.depends('employees')
    def on_change_with_allowed_employees(self, name='allowed_employees'):
        if self.employees:
            employees = [e.employee for e in self.employees if e.valid()]
            return list(set(map(int, employees)))
        return []

    @classmethod
    def get_have_employees(cls, records, name=None):
        pool = Pool()
        TTsEmployee = pool.get('timesheet.team.timesheet-company.employee')
        tts_employee = TTsEmployee.__table__()
        cursor = Transaction().connection.cursor()

        ts_ids = list(map(int, records))
        query = tts_employee.select(
            tts_employee.team_timesheet,
            where=(tts_employee.team_timesheet.in_(ts_ids)),
            group_by=tts_employee.team_timesheet
        )
        cursor.execute(*query)
        query_result = cursor.fetchall()
        result = {qr[0]: True for qr in query_result}
        for r in ts_ids:
            result.setdefault(r, False)
        return result

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, timesheets):
        pool = Pool()
        TimesheetLine = pool.get('timesheet.line')
        timesheet_lines = [timesheet for team_timesheet in timesheets
            for timesheet in team_timesheet.timesheets]

        if timesheet_lines:
            with Transaction().set_context(allow_delete_line=True):
                TimesheetLine.delete(timesheet_lines)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, timesheets):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        User = pool.get('res.user')
        Group = pool.get('res.group')
        Line = pool.get('timesheet.line')

        def in_group():
            group = Group(ModelData.get_id('timesheet',
                'group_timesheet_admin'))
            transaction = Transaction()
            user_id = transaction.user
            if user_id == 0:
                user_id = transaction.context.get('user', user_id)
            if user_id == 0:
                return True
            user = User(user_id)
            return group in user.groups

        done_timesheets = [r for r in timesheets if r.state == 'done']

        if done_timesheets and not in_group():
            raise UserError(gettext(
                'team_timesheet.msg_timesheet_team_timesheet_cancel_done'))

        lines = [line for timesheet in timesheets
            for line in timesheet.timesheets]
        Line.delete(lines)

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting')
    def wait(cls, timesheets):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, timesheets):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def do(cls, timesheets):
        pool = Pool()
        Date = pool.get('ir.date')
        TimesheetWorkEmployee = pool.get(
            'timesheet.team.work-company.employee')

        cls.check_unique_ts(timesheets)
        TimesheetWorkEmployee.create_timesheets(timesheets)

        cls.write(timesheets, {'done_date': Date.today()})

    @classmethod
    def check_unique_ts(cls, timesheets):
        pool = Pool()
        Lang = pool.get('ir.lang')
        Warning = pool.get('res.user.warning')

        my_dictionary = {}
        for timesheet in timesheets:
            if not timesheet.team:
                continue
            if my_dictionary.get((timesheet.date, timesheet.team), None):
                duplicated = True
            else:
                my_dictionary[(timesheet.date, timesheet.team)] = timesheet.id
                duplicated = cls.search([
                    ('team', '=', timesheet.team),
                    ('date', '=', timesheet.date),
                    ('id', '!=', timesheet.id),
                    ('state', '=', 'done')])
            if duplicated:
                language = Transaction().language
                languages = Lang.search([('code', '=', language)])
                if not languages:
                    languages = Lang.search([('code', '=', 'en_US')])
                language, = languages
                formatted = language.strftime(timesheet.date)
                warning_name = 'duplicated_timesheet_%s_%s' % (timesheet.team,
                    formatted)
                if Warning.check(warning_name):
                    raise UserWarning(warning_name, gettext(
                        'team_timesheet.msg_timesheet_team_timesheet_duplicated_timesheet',
                        team=timesheet.team.rec_name,
                        date=formatted))

    def check_timesheet_changes(self, line):
        if self.state in {'confirmed', 'done'}:
            raise UserError(gettext(
                'team_timesheet.msg_timesheet_line_modify_line_tts_confirmed_done',
                line=line.rec_name,
                sheet=self.rec_name))
        return True

    @classmethod
    def _create_tts_work_employee(cls, tts_work, tts_employee):
        pool = Pool()
        TimesheetWorkEmployee = pool.get(
            'timesheet.team.work-company.employee')

        twe = TimesheetWorkEmployee(
            tts_work=tts_work,
            tts_employee=tts_employee,
        )
        twe.work = twe.get_work()
        twe.employee = twe.get_employee()

        if tts_work.time_type == 'employee':
            twe.duration = tts_work.duration
        else:
            twe.duration = timedelta(seconds=tts_work.duration.total_seconds()
                / len(tts_work.team_timesheet.employees))
        return twe

    @classmethod
    def create_tts_work_employee(cls, tts_works, tts_employees):
        pool = Pool()
        TimesheetWorkEmployee = pool.get(
            'timesheet.team.work-company.employee')

        to_save = []
        for tts_work in tts_works:
            for tts_employee in tts_employees:
                twe = cls._create_tts_work_employee(
                    tts_work, tts_employee)
                to_save.append(twe)

        if to_save:
            TimesheetWorkEmployee.save(to_save)

    @classmethod
    def write(cls, *args):
        pool = Pool()
        TimesheetLine = pool.get('timesheet.line')

        actions = iter(args)
        to_write = []
        for records, values in zip(actions, actions):
            if values.get('date'):
                timesheets = [t for r in records for t in r.timesheets]
                to_write.extend((timesheets, {'date': values['date']}))

        if to_write:
            TimesheetLine.write(*to_write)

        super().write(*args)

    @classmethod
    def edit_tts_work_employee_domain(cls, tts_works, tts_employees):
        domain = [
            ('tts_work', 'in', tts_works),
            ('tts_employee', 'in', tts_employees),
        ]

        return domain

    @classmethod
    def edit_tts_work_employee(cls, tts_works, tts_employees, **kwargs):
        pool = Pool()
        TimesheetWorkEmployee = pool.get(
            'timesheet.team.work-company.employee')

        to_save = []
        modify_duration = 'time_type' in kwargs or 'duration' in kwargs
        _time_type = kwargs.pop('time_type', None)
        _duration = kwargs.pop('duration', None)
        tts_work_employees = TimesheetWorkEmployee.search(
            cls.edit_tts_work_employee_domain(tts_works, tts_employees))

        for twe in tts_work_employees:
            if modify_duration:
                time_type = _time_type or twe.tts_work.time_type
                duration = _duration or twe.tts_work.duration
                if time_type == 'employee':
                    twe.duration = duration
                if time_type == 'team':
                    twe.duration = timedelta(
                        seconds=duration.total_seconds() / len(
                            tts_work_employees))

            for key, value in kwargs.items():
                if hasattr(twe, key):
                    setattr(twe, key, value)

            to_save.append(twe)

        if to_save:
            TimesheetWorkEmployee.save(to_save)


class TeamTimesheetDetail(object):
    team_timesheet = fields.Many2One('timesheet.team.timesheet',
        'Team Timesheet', ondelete='CASCADE', required=True, select=True,
        states={
            'readonly': (
                (Eval('tts_state') != 'draft')
                & Bool(Eval('team_timesheet'))),
        },
        depends=['tts_state'])
    tts_state = fields.Function(fields.Selection('_get_tts_states',
        'Team timesheet State'), 'on_change_with_tts_state')

    @classmethod
    def delete(cls, records):
        pool = Pool()
        Warning = pool.get('res.user.warning')

        tss = [ts for r in records
               for ts in r.timesheets if r.timesheets]
        if tss and cls._must_warn(records):
            warning_name = '%s.cannot_delete' % 0
            if Warning.check(warning_name):
                raise UserWarning(warning_name, gettext(
                    'team_timesheet.msg_timesheet_team_timesheet-timesheet_work_cannot_delete'))
            Timesheet = Pool().get('timesheet.line')
            Timesheet.delete(tss)

        super(TeamTimesheetDetail, cls).delete(records)

    @classmethod
    def _must_warn(cls, records):
        return True

    @classmethod
    def _get_tts_states(cls):
        return Pool().get('timesheet.team.timesheet').state.selection

    @fields.depends('team_timesheet', '_parent_team_timesheet.state')
    def on_change_with_tts_state(self, name=None):
        if self.team_timesheet:
            return self.team_timesheet.state


class TeamTimesheetWork(TeamTimesheetDetail, ModelView, ModelSQL):
    """TeamTimesheet - Work"""
    __name__ = 'timesheet.team.timesheet-timesheet.work'
    _table = 'timesheet_team_work_rel'

    team_works = fields.Function(
        fields.Many2Many('timesheet.work', None, None, 'Team works'),
        'get_team_works')
    work = fields.Many2One('timesheet.work', 'Work',
        ondelete='CASCADE', required=True, select=True,
        domain=[
            ('company', '=', Eval('_parent_team_timesheet',
                {}).get('company', None)),
            If(Bool(Eval('team_works', [])),
                ('id', 'in', Eval('team_works', [])),
                ())
        ],
        states={
            'readonly': (
                    Bool(Eval('timesheets'))
                    | (Eval('tts_state') != 'draft'))
        },
        depends=['timesheets', 'team_works', 'tts_state'])
    time_type = fields.Selection(
        [('team', 'Team time'),
         ('employee', 'Employee time'), ],
        'Time type', required=True,
        states={'readonly': (Eval('tts_state') != 'draft')},
        depends=['tts_state'])
    duration = fields.TimeDelta('Duration', converter='absolute_hours_time',
        required=True, states={
            'readonly': (Eval('tts_state') != 'draft')
        }, depends=['tts_state'])
    total_duration = fields.Function(
        fields.TimeDelta('Total duration', converter='absolute_hours_time'),
        'get_total_duration')
    planned_duration = fields.Function(
        fields.TimeDelta('Planned duration', converter='absolute_hours_time'),
        'on_change_with_planned_duration')
    timesheets = fields.One2Many('timesheet.line', 'tts_work',
        'Timesheet lines',
        order=[('employee', 'ASC')],
        domain=[('work', '=', Eval('work'))],
        states={'readonly': (Eval('tts_state') != 'draft')},
        depends=['work', 'tts_state'])
    tts_date = fields.Function(
        fields.Date('Date'),
        'on_change_with_tts_date', searcher='search_tts_date')
    total_hours = fields.Function(
        fields.Float('Total hours'),
        'get_total_hours')

    @classmethod
    def __setup__(cls):
        super(TeamTimesheetWork, cls).__setup__()
        cls._buttons.update({
            'edit_work_timesheets': {
                'readonly': (Eval('_parent_team_timesheet', {}).
                        get('state').in_(['confirmed', 'done', 'cancelled'])),
                'invisible': (
                    Bool(Eval('_parent_team_timesheet.num_workers'))
                    | Eval('_parent_team_timesheet', {}
                        ).get('state').in_(['confirmed', 'done', 'cancelled'])
                )
            }
        })

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)

        # Migration from 3.6: reduce table name length
        old_table = 'timesheet_team_timesheet-timesheet_work'
        if table_h.table_exist(old_table):
            table_h.table_rename(old_table, cls._table)

        if table_h.column_exist('hours'):
            table_h.column_rename('hours', 'duration')
        super(TeamTimesheetWork, cls).__register__(module_name)

        # Drop TeamTimesheet - Work UK
        table_h.drop_constraint('work_uk1', table=old_table)
        table_h.drop_constraint('work_uk1')

    @classmethod
    def copy(cls, works, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['timesheets'] = None
        return super(TeamTimesheetWork, cls).copy(works, default=default)

    @staticmethod
    def default_time_type():
        return 'employee'

    @fields.depends('team_timesheet', '_parent_team_timesheet.team',
        'team_works')
    def on_change_team_timesheet(self):
        if self.team_timesheet and self.team_timesheet.team:
            self.work = self.team_timesheet.team.get_def_ts_available_work()

    @fields.depends('team_timesheet', '_parent_team_timesheet.team')
    def on_change_with_team_works(self):
        if getattr(self.team_timesheet, 'team', None):
            return self.get_team_works(None)
        return []

    @classmethod
    def get_total_duration(cls, records, name):
        pool = Pool()
        TimesheetWorkEmployee = pool.get(
            'timesheet.team.work-company.employee')

        twe = TimesheetWorkEmployee.__table__()
        cursor = Transaction().connection.cursor()

        res = {r.id: timedelta(0) for r in records}
        records_ids = []
        for record in records:
            if record.team_timesheet.num_workers:
                res[record.id] = record.planned_duration
            elif record.team_timesheet.state != 'done':
                records_ids.append(record.id)
            else:
                seconds = sum([ts.duration.total_seconds()
                    for ts in record.timesheets if ts.duration])
                res[record.id] = timedelta(seconds=seconds)

        if records_ids:
            cursor.execute(*twe.select(
                twe.tts_work,
                Sum(twe.duration),
                where=reduce_ids(twe.tts_work, records_ids),
                group_by=twe.tts_work))

            for record_id, duration in cursor.fetchall():
                if backend.name == 'sqlite':
                    duration = timedelta(seconds=duration)
                res[record_id] = duration

        return res

    def get_total_hours(self, name=None):
        if self.total_duration:
            return self.total_duration.total_seconds() / 60 / 60

        return None

    @fields.depends('time_type', 'duration', 'team_timesheet',
        '_parent_team_timesheet.employees',
        '_parent_team_timesheet.num_workers')
    def on_change_with_planned_duration(self, name=None):
        if self.duration and self.time_type:
            if self.time_type == 'team':
                return self.duration
            if (self.time_type == 'employee' and self.team_timesheet and
                    self.team_timesheet.employees):
                return self.duration * len(self.team_timesheet.employees)
            if (self.time_type == 'employee' and
                    self.team_timesheet.num_workers):
                return self.duration * self.team_timesheet.num_workers
        return timedelta(hours=0)

    @fields.depends('team_timesheet', '_parent_team_timesheet.date')
    def on_change_with_tts_date(self, name=None):
        if self.team_timesheet:
            return self.team_timesheet.date

    @classmethod
    def search_tts_date(cls, name, clause):
        return [('team_timesheet.date', ) + tuple(clause[1:])]

    # TODO: searcher method
    def get_rec_name(self, name):
        return self.team_timesheet.rec_name + ' - ' + self.work.rec_name

    @classmethod
    @ModelView.button_action('team_timesheet.wizard_edit_work_timesheets')
    def edit_work_timesheets(cls, works):
        pass

    def _get_unique_key(self):
        return (self.work.id, )

    @classmethod
    def validate(cls, records):
        super(TeamTimesheetWork, cls).validate(records)

        # Ensure teamtimesheet works unique criteria
        cls._check_unique_key(records)

    @classmethod
    def _check_unique_key(cls, records):
        pool = Pool()
        Ttsw = pool.get('timesheet.team.timesheet-timesheet.work')

        records.sort(key=lambda x: x.team_timesheet)
        for tts, tts_works in groupby(records,
                key=lambda x: x.team_timesheet):
            tts_worksl = list(tts_works)
            tts_works_db = Ttsw.search([
                ('team_timesheet', '=', tts),
                ('id', 'not in', list(map(int, tts_worksl)))])
            tts_worksl.extend(tts_works_db)
            tts_worksl.sort(key=lambda x: x._get_unique_key())
            for unq_key, uk_ttsws in groupby(tts_worksl,
                    key=lambda x: x._get_unique_key()):
                uk_ttswsl = list(uk_ttsws)
                if len(uk_ttswsl) > 1:
                    raise UserError(gettext(
                        'team_timesheet.msg_timesheet_team_timesheet-timesheet_work_unique_key',
                        work=uk_ttswsl[0].work.rec_name,
                        sheet=uk_ttswsl[0].team_timesheet.rec_name))

    def get_team_works(self, name):
        return (self.team_timesheet.team and self.team_timesheet.team.works and
            list(map(int, self.team_timesheet.team.works)) or [])

    def _update_date(self, date):
        for line in self.timesheets:
            line.date = date

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        TimesheetTeam = pool.get('timesheet.team.timesheet')

        records = super().create(vlist)

        records.sort(key=lambda x: x.team_timesheet)
        for team_timesheet, tts_works in groupby(records,
                key=lambda x: x.team_timesheet):
            TimesheetTeam.create_tts_work_employee(
                list(tts_works), team_timesheet.employees)

        return records

    @classmethod
    def write(cls, *args):
        Timesheet = Pool().get('timesheet.team.timesheet')

        actions = iter(args)
        for tts_works, values in zip(actions, actions):
            tts_works = sorted(tts_works, key=lambda x: x.team_timesheet)
            for team_timesheet, tts_works in groupby(tts_works,
                    key=lambda x: x.team_timesheet):
                kwargs = {}
                if 'duration' in values:
                    kwargs['duration'] = values['duration']
                if 'time_type' in values:
                    kwargs['time_type'] = values['time_type']
                if kwargs:
                    Timesheet.edit_tts_work_employee(list(tts_works),
                        team_timesheet.employees, **kwargs)

        super().write(*args)


class TeamTimesheetEmployee(EmployeeOrderMixin, ModelView, ModelSQL,
        TeamTimesheetDetail):
    """TeamTimesheet - Employee"""
    __name__ = 'timesheet.team.timesheet-company.employee'
    _table = 'timesheet_team_employee_rel'

    employee = fields.Many2One('company.employee', 'Employee',
        ondelete='CASCADE', required=True, select=True,
        domain=[
            ('company', '=', Eval('_parent_team_timesheet',
                {}).get('company', None)),
            ['OR',
                ('start_date', '=', None),
                ('start_date', '<=', Eval('_parent_team_timesheet',
                    {}).get('date'))],
            ['OR',
                ('end_date', '=', None),
                ('end_date', '>=', Eval('_parent_team_timesheet',
                    {}).get('date'))]
                ],
        states={
            'readonly': (
                    Bool(Eval('timesheets'))
                    | (Eval('tts_state') != 'draft'))
        },
        depends=['team_timesheet', 'timesheets', 'tts_state'])
    duration = fields.Function(
        fields.TimeDelta('Duration', 'absolute_hours_time'),
        'get_duration')
    day_duration = fields.Function(
        fields.TimeDelta('Day duration', 'absolute_hours_time'),
        'on_change_with_day_duration')
    timesheets = fields.Function(
        fields.Many2Many('timesheet.line', None, None, 'Timesheets',
            readonly=False, required=False),
        'get_timesheets')
    total_hours = fields.Function(
        fields.Float('Hours'),
        'get_total_hours')
    total_day_hours = fields.Function(
        fields.Float('Day hours'),
        'on_change_with_total_day_hours')

    @classmethod
    def __setup__(cls):
        super(TeamTimesheetEmployee, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('employee_uk1', Unique(t, t.team_timesheet, t.employee),
                'team_timesheet.msg_timesheet_team_timesheet-company_employee_employee_uk1')]

        cls._buttons.update({
            'edit_employee_timesheets': {
                'readonly': Eval('_parent_team_timesheet',
                    {}).get('state').in_(['confirmed', 'done', 'cancelled']),
                'invisible': Eval('_parent_team_timesheet',
                    {}).get('state').in_(['confirmed', 'done', 'cancelled']),
            }
        })

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)

        # Migration from 3.6: reduce table name length
        old_table = 'timesheet_team_timesheet-company_employee'
        if table_h.table_exist(old_table):
            table_h.table_rename(old_table, cls._table)

        super(TeamTimesheetEmployee, cls).__register__(module_name)

    def get_timesheets(self, name=None):
        if self.team_timesheet and self.employee:
            if self.team_timesheet.timesheets:
                return [ts.id for ts in self.team_timesheet.timesheets
                    if ts.employee and ts.employee == self.employee]
        return None

    @classmethod
    def get_duration(cls, records, name):
        pool = Pool()
        TimesheetWorkEmployee = pool.get(
            'timesheet.team.work-company.employee')
        twe = TimesheetWorkEmployee.__table__()
        cursor = Transaction().connection.cursor()

        res = {r.id: timedelta(0) for r in records}
        records_ids = []
        for record in records:
            if record.team_timesheet.state == 'done':
                seconds = sum([ts.duration.total_seconds()
                    for ts in record.timesheets if ts.duration])
                res[record.id] = timedelta(seconds=seconds)
            else:
                records_ids.append(record.id)

        if records_ids:
            cursor.execute(*twe.select(
                twe.tts_employee,
                Sum(twe.duration),
                where=reduce_ids(twe.tts_employee, records_ids),
                group_by=(twe.tts_employee)))
            for record_id, duration in cursor.fetchall():
                if backend.name == 'sqlite':
                    duration = timedelta(seconds=duration)
                res[record_id] = duration

        return res

    @classmethod
    def get_total_hours(cls, records, name):
        res = cls.get_duration(records, name)
        for key, value in res.items():
            res[key] = value.total_seconds() / 60 / 60
        return res

    @fields.depends('team_timesheet', '_parent_team_timesheet.date',
        'duration', 'employee')
    def on_change_with_day_duration(self, name=None):
        if self.employee:
            return (
                self.employee.compute_day_hours(self.team_timesheet.date,
                    team_timesheet=self.team_timesheet) + (
                    self.duration or timedelta(hours=0))
            )
        return timedelta(hours=0)

    @fields.depends(methods=['on_change_with_day_duration'])
    def on_change_with_total_day_hours(self, name=None):
        return self.on_change_with_day_duration().total_seconds() / 60 / 60

    @classmethod
    @ModelView.button_action('team_timesheet.wizard_edit_employee_timesheets')
    def edit_employee_timesheets(cls, employees):
        pass

    @classmethod
    def delete(cls, employees):
        pool = Pool()
        Line = pool.get('timesheet.line')
        timesheets = []
        for employee in employees:
            timesheets.extend(employee.timesheets)
        Line.delete(timesheets)
        super(TeamTimesheetEmployee, cls).delete(employees)

    def valid(self):
        return bool(self.employee)

    # TODO: searcher method
    def get_rec_name(self, name):
        if Transaction().context.get('only_employee_name', False):
            return self.employee.rec_name
        return '%s - %s' % (self.team_timesheet.rec_name,
            self.employee.rec_name)

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        TimesheetTeam = pool.get('timesheet.team.timesheet')

        records = super().create(vlist)

        records.sort(key=lambda x: x.team_timesheet)
        for team_timesheet, tts_employes in groupby(records,
                key=lambda x: x.team_timesheet):
            TimesheetTeam.create_tts_work_employee(
                team_timesheet.works, list(tts_employes))

        return records


class EditTimesheetsStart(ModelView):
    team_timesheet = fields.Many2One('timesheet.team.timesheet',
        'Team Timesheet', readonly=True)
    date = fields.Date('Date', readonly=True)
    team = fields.Many2One('company.employee.team', 'Team', readonly=True)
    timesheets = fields.One2Many('timesheet.team.work-company.employee', None,
        'Timesheets', readonly=True)
    go_next = fields.Boolean('Go next')
    go_previous = fields.Boolean('Go previous')


class EditTimesheets(Wizard):

    edit_timesheets = StateTransition()

    def transition_edit_timesheets(self):
        for ts in self.start.timesheets:
            ts.save()

        return 'end'

    def default_start(self, fields):
        DetModel = Pool().get(Transaction().context['active_model'])
        instance = DetModel(Transaction().context['active_id'])

        record = dict(
            team_timesheet=instance.team_timesheet.id,
            date=instance.team_timesheet.date,
            team=instance.team_timesheet.team.id
        )

        return record

    def _go_to_record(self, action, direction=1):
        # save current
        self.transition_edit_timesheets()
        index = self.start.records.index(self.record) + direction
        if index < 0:
            index = len(self.start.records) - 1
        elif index >= len(self.start.records):
            index = 0
        new_record = self.start.records[index]
        data = {
            'model': self.record.__name__,
            'id': new_record.id,
            'ids': [new_record.id],
        }
        return action, data

    def do_next_(self, action):
        return self._go_to_record(action)

    def do_previous_(self, action):
        return self._go_to_record(action, -1)


class EditWorkTimesheetStart(EditTimesheetsStart):
    """Edit team's timesheet of a work"""
    __name__ = (
        'timesheet.team.timesheet-timesheet.work.edit_timesheets.start')

    work = fields.Many2One('timesheet.work', 'Work', readonly=True)
    duration = fields.TimeDelta('Duration', 'company_work_time', required=True)
    planned_duration = fields.TimeDelta('Planed duration', readonly=True,
        converter='absolute_hours_time')
    allowed_employees = fields.One2Many('company.employee', None,
        'Allowed employees', readonly=True, required=False)
    time_type = fields.Selection(
        [('team', 'Team time'),
         ('employee', 'Employee time'), ],
        'Time type', required=False, readonly=True)
    total_duration = fields.TimeDelta('Total duration', readonly=True,
        converter='absolute_hours_time')
    records = fields.Many2Many('timesheet.team.timesheet-timesheet.work',
        None, None, 'Works')

    @fields.depends('timesheets')
    def on_change_with_total_duration(self):
        seconds = 0
        if self.timesheets:
            seconds = sum([twe.duration.total_seconds()
                for twe in self.timesheets if twe.duration])

        return timedelta(seconds=seconds)

    @fields.depends('duration', 'timesheets', 'time_type',
        methods=['on_change_with_total_duration'])
    def on_change_duration(self):
        duration = self.duration or timedelta(0)
        if self.time_type == 'employee':
            self.planned_duration = timedelta(seconds=duration.total_seconds()
                * len(self.timesheets))
        else:
            self.planned_duration = duration
        for twe in self.timesheets:
            if self.time_type == 'employee':
                twe.duration = duration
            else:
                twe.duration = timedelta(seconds=duration.total_seconds()
                    / len(self.timesheets))
            twe.on_change_duration()

        self.total_duration = self.on_change_with_total_duration()

    # avoid first execution
    def on_change(self, fieldnames):
        pool = Pool()
        ModelData = pool.get('ir.model.data')

        context = Transaction().context
        wizard_edit_timesheets_id = ModelData.get_id(
            'team_timesheet', 'wizard_edit_work_timesheets')
        if (len(fieldnames) > 1
                and context.get('action_id') == wizard_edit_timesheets_id):
            return []

        return super().on_change(fieldnames)


class EditWorkTimesheets(EditTimesheets):
    """Edit team's timesheet of a work"""
    __name__ = 'timesheet.team.timesheet-timesheet.work.edit_timesheets'

    start = StateView(
        'timesheet.team.timesheet-timesheet.work.edit_timesheets.start',
        'team_timesheet.edit_work_timesheets_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Previous', 'previous_', 'tryton-back',
                states={
                    'readonly': ~Eval('go_previous'),
                }),
            Button('Next', 'next_', 'tryton-forward', default=True,
                states={
                    'readonly': ~Eval('go_next'),
                }),
            Button('Update', 'edit_timesheets', 'tryton-ok'),
        ])
    next_ = StateAction('team_timesheet.wizard_edit_work_timesheets')
    previous_ = StateAction('team_timesheet.wizard_edit_work_timesheets')

    def timesheet_work_employee_domain(self):
        domain = [
            ('tts_work', '=', self.record),
            ('team_timesheet', '=', self.record.team_timesheet)]

        return domain

    def default_start(self, fields):
        pool = Pool()
        TimesheetWorkEmployee = pool.get(
            'timesheet.team.work-company.employee')

        super_record = super().default_start(fields)

        domain = self.timesheet_work_employee_domain()
        timesheets = TimesheetWorkEmployee.search(domain)

        record = dict(
            work=self.record.work.id,
            duration=self.record.duration,
            planned_duration=self.record.planned_duration,
            total_duration=timedelta(seconds=sum([twe.duration.total_seconds()
                for twe in timesheets])),
            time_type=self.record.time_type,
            allowed_employees=[e.id
                for e in self.record.team_timesheet.allowed_employees],
            timesheets=list(map(int, timesheets))
        )
        super_record.update(record)
        self.start.records = list(self.record.team_timesheet.works)
        super_record['go_next'] = bool(self.start.records.index(self.record)
            < (len(self.start.records) - 1))
        super_record['go_previous'] = bool(
            self.start.records.index(self.record) > 0)
        return super_record

    def transition_edit_timesheets(self):
        if self.record.duration != self.start.duration:
            self.record.duration = self.start.duration
            self.record.save()

        return super().transition_edit_timesheets()


class EditEmployeeTimesheetsStart(EditTimesheetsStart):
    """Edit team's timesheet of a employee"""
    __name__ = (
        'timesheet.team.timesheet-company.employee.edit_timesheets.start')

    employee = fields.Many2One('company.employee', 'Employee', readonly=True)
    day_duration = fields.TimeDelta('Day duration',
        converter='absolute_hours_time', readonly=True)
    allowed_works = fields.One2Many('timesheet.work', None, 'Allowed works',
        readonly=True, required=False)
    records = fields.Many2Many('timesheet.team.timesheet-company.employee',
        None, None, 'Employees')


class EditEmployeeTimesheets(EditTimesheets):
    """Edit team's timesheet of a employee"""
    __name__ = 'timesheet.team.timesheet-company.employee.edit_timesheets'

    start = StateView(
        'timesheet.team.timesheet-company.employee.edit_timesheets.start',
        'team_timesheet.edit_employee_timesheets_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Previous', 'previous_', 'tryton-back',
                states={
                    'readonly': ~Eval('go_previous'),
                }),
            Button('Next', 'next_', 'tryton-forward', default=True,
                states={
                    'readonly': ~Eval('go_next'),
                }),
            Button('Update', 'edit_timesheets', 'tryton-ok')])
    next_ = StateAction('team_timesheet.wizard_edit_employee_timesheets')
    previous_ = StateAction('team_timesheet.wizard_edit_employee_timesheets')

    def timesheet_work_employee_domain(self):
        domain = [
            ('tts_employee', '=', self.record),
            ('team_timesheet', '=', self.record.team_timesheet)]

        return domain

    def default_start(self, fields):
        pool = Pool()
        TimesheetWorkEmployee = pool.get(
            'timesheet.team.work-company.employee')

        super_record = super().default_start(fields)

        domain = self.timesheet_work_employee_domain()
        timesheets = TimesheetWorkEmployee.search(domain)

        record = dict(
            employee=self.record.employee.id,
            day_duration=self.record.day_duration,
            allowed_works=[w.id for w in
                self.record.team_timesheet.allowed_works],
            timesheets=list(map(int, timesheets)))

        super_record.update(record)
        self.start.records = list(self.record.team_timesheet.employees)
        super_record['go_next'] = bool(self.start.records.index(self.record)
            < (len(self.start.records) - 1))
        super_record['go_previous'] = bool(
            self.start.records.index(self.record) > 0)
        return super_record


class TimesheetTeamWorkCompanyEmployee(ModelSQL, ModelView):
    """Timesheet Team Work - Company Employee"""
    __name__ = 'timesheet.team.work-company.employee'

    team_timesheet = fields.Function(
        fields.Many2One('timesheet.team.timesheet', 'Team timesheet'),
        'get_team_timesheet', searcher='search_team_timesheet')
    tts_work = fields.Many2One('timesheet.team.timesheet-timesheet.work',
        'Team timesheet work', select=True, required=True, readonly=True,
        ondelete='CASCADE')
    tts_employee = fields.Many2One('timesheet.team.timesheet-company.employee',
        'Team timesheet employee', select=True, required=True, readonly=True,
        ondelete='CASCADE')
    work = fields.Function(
        fields.Many2One('timesheet.work', 'Work'),
        'get_work', searcher='search_work')
    employee = fields.Function(
        fields.Many2One('company.employee', 'Employee'),
        'get_employee', searcher='search_employee')
    timesheet_line = fields.Many2One('timesheet.line', 'Timesheet line',
        readonly=True)
    duration = fields.TimeDelta('Duration', converter='absolute_hours_time',
        required=True)
    total_hours = fields.Function(
        fields.Float('Hours'),
        'get_total_hours', setter='set_total_hours')
    currency = fields.Function(
        fields.Many2One('currency.currency', 'Currency'),
        'get_currency')
    currency_digits = fields.Function(
        fields.Integer('Currency digits'),
        'get_currency_digits')
    tts_date = fields.Function(fields.Date('Date'),
        'get_tts_date', searcher='search_tts_date')

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        TTSEmployee = pool.get('timesheet.team.timesheet-company.employee')
        TeamTimesheet = pool.get('timesheet.team.timesheet')
        TTSWork = pool.get('timesheet.team.timesheet-timesheet.work')
        TimesheetLine = pool.get('timesheet.line')

        sql_table = cls.__table__()
        tts_employee = TTSEmployee.__table__()
        team_timesheet = TeamTimesheet.__table__()
        tts_work = TTSWork.__table__()
        timesheet_line = TimesheetLine.__table__()

        cursor = Transaction().connection.cursor()
        exist = backend.TableHandler.table_exist(cls._table)

        super().__register__(module_name)

        if not exist:
            with_ = []
            subquery_line = With()
            subquery_line.query = team_timesheet.join(tts_work,
                    condition=(tts_work.team_timesheet == team_timesheet.id)
                ).join(timesheet_line,
                    condition=(timesheet_line.tts_work == tts_work.id)
                ).select(
                    team_timesheet.id.as_('team_timesheet'),
                    group_by=[team_timesheet.id]
                )
            with_.append(subquery_line)

            subquery_employees = With()
            subquery_employees.query = tts_employee.join(team_timesheet,
                condition=(team_timesheet.id == tts_employee.team_timesheet)
                ).select(
                    team_timesheet.id.as_('team_timesheet'),
                    Count(Literal('*')).as_('len'),
                    group_by=[team_timesheet.id]
                )
            with_.append(subquery_employees)

            query_values = tts_employee.join(team_timesheet,
                    condition=(
                        team_timesheet.id == tts_employee.team_timesheet)
                ).join(tts_work,
                    condition=(tts_work.team_timesheet == team_timesheet.id)
                ).join(subquery_line, 'LEFT',
                    condition=(
                        subquery_line.team_timesheet == team_timesheet.id)
                ).join(subquery_employees,
                    condition=(
                        subquery_employees.team_timesheet == team_timesheet.id)
                ).join(timesheet_line, 'LEFT',
                    condition=((timesheet_line.tts_work == tts_work.id)
                        & (timesheet_line.employee == tts_employee.employee))
                ).select(
                    tts_work.id.as_('tts_work'),
                    tts_employee.id.as_('tts_employee'),
                    timesheet_line.id.as_('timesheet_line'),
                    Case(
                        ((timesheet_line.id == Null)
                            & (subquery_line.team_timesheet != Null),
                            Literal('0')),
                        ((timesheet_line.id == Null)
                            & (tts_work.time_type == 'employee'),
                            tts_work.duration),
                        ((timesheet_line.id == Null)
                            & (tts_work.time_type == 'team'),
                            tts_work.duration / subquery_employees.len),
                        else_=timesheet_line.duration).as_('duration'),
                    team_timesheet.create_uid,
                    team_timesheet.create_date,
                    team_timesheet.write_date,
                    team_timesheet.write_uid,
                    with_=with_)

            cursor.execute(*sql_table.insert(
                columns=[
                    sql_table.tts_work,
                    sql_table.tts_employee,
                    sql_table.timesheet_line,
                    sql_table.duration,
                    sql_table.create_uid,
                    sql_table.create_date,
                    sql_table.write_date,
                    sql_table.write_uid
                ],
                values=query_values))

            # Delete timesheet lines
            delete_lines = team_timesheet.join(tts_work,
                    condition=(tts_work.team_timesheet == team_timesheet.id)
                ).join(timesheet_line,
                    condition=(timesheet_line.tts_work == tts_work.id)
                ).select(
                    timesheet_line.id,
                    where=(team_timesheet.state != 'done')
                )

            cursor.execute(*delete_lines)
            delete_lines_ids = [x[0] for x in cursor.fetchall()]
            if delete_lines_ids:
                cursor.execute(*timesheet_line.delete(
                    where=reduce_ids(timesheet_line.id, delete_lines_ids)))

    def get_currency(self, name):
        if self.team_timesheet:
            return self.team_timesheet.company.currency.id

        return None

    def get_currency_digits(self, name=None):
        if self.team_timesheet:
            return self.team_timesheet.company.currency.digits

        return 2

    @fields.depends('duration')
    def get_total_hours(self, name=None):
        if self.duration:
            return self.duration.total_seconds() / 60 / 60

        return None

    def get_team_timesheet(self, name=None):
        if self.tts_work and self.tts_work.team_timesheet:
            return self.tts_work.team_timesheet.id

        return None

    def get_tts_date(self, name=None):
        if self.team_timesheet:
            return self.team_timesheet.date

        return None

    def get_work(self, name=None):
        if self.tts_work and self.tts_work.work:
            return self.tts_work.work.id

        return None

    def get_employee(self, name=None):
        if self.tts_employee and self.tts_employee.employee:
            return self.tts_employee.employee.id

        return None

    @fields.depends(methods=['get_total_hours'])
    def on_change_duration(self):
        self.total_hours = self.get_total_hours()

    @fields.depends('total_hours', methods=['on_change_duration'])
    def on_change_total_hours(self):
        if self.total_hours is not None:
            self.duration = timedelta(hours=self.total_hours)
            self.on_change_duration()
        else:
            self.duration = None

    @classmethod
    def set_total_hours(cls, lines, name, value):
        pass

    @classmethod
    def search_team_timesheet(cls, name, clause):
        return [('tts_work.' + clause[0],) + tuple(clause[1:])]

    @classmethod
    def search_work(cls, name, clause):
        return [('tts_work.' + clause[0],) + tuple(clause[1:])]

    @classmethod
    def search_employee(cls, name, clause):
        return [('tts_employee.' + clause[0],) + tuple(clause[1:])]

    @classmethod
    def search_tts_date(cls, name, clause):
        return [('team_timesheet.date',) + tuple(clause[1:])]

    @classmethod
    def _create_timesheets(cls, record):
        pool = Pool()
        TimesheetLine = pool.get('timesheet.line')
        line = TimesheetLine(
            company=record.employee.company,
            work=record.work,
            employee=record.employee,
            duration=record.duration,
            tts_work=record.tts_work,
            date=record.team_timesheet.date)

        return line

    @classmethod
    def create_timesheets(cls, team_timesheets):
        records = cls.search(
            [('team_timesheet', 'in', team_timesheets)])

        for record in records:
            if record.duration and record.duration > timedelta(0):
                line = cls._create_timesheets(record)
                record.timesheet_line = line

        if records:
            cls.save(records)
