# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from datetime import timedelta

__all__ = ['Employee', 'Team']


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    def compute_day_hours(self, date, operation='exclude',
            team_timesheet=None):
        domain = [
            ('date', '=', date),
            ('employee', '=', self.id)]
        if team_timesheet:
            if operation == 'exclude':
                domain.append(['OR',
                    ('team_timesheet', '!=', team_timesheet.id),
                    ('team_timesheet', '=', None)])
            else:
                domain.append(('team_timesheet', '=', team_timesheet.id))

        Timesheet = Pool().get('timesheet.line')
        timesheets = Timesheet.search(domain)
        result = sum(t.duration.total_seconds() for t in timesheets)
        return timedelta(seconds=result)


class Team(metaclass=PoolMeta):
    __name__ = 'company.employee.team'

    works = fields.Many2Many('timesheet.work-company.employee.team',
        'team', 'work', 'Works')

    def get_def_ts_available_work(self):
        works = [w for w in self.works or []]
        return works[0] if len(works) == 1 else None
